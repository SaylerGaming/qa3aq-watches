<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/css/main.css">
    <title>Watches</title>
</head>
<body>
<div class="contacts">
    <div class="contacts__wrapper container">
        <div class="contacts__time">
            Работаем 24/7
        </div>
        <div class="contacts__socials">
            <div class="contacts__images">
                @foreach ($socials as $social)
                <div class="contacts__image">
                    <a href="{{ $social->name }}"><img src="/storage/{{ $social->image }}" alt=""></a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<header class="header">
    <div class="container">
        <div class="header__info">
            <div class="header__logo">
                <img src="images/logo.png" alt="">
            </div>
            <div class="header__contacts">
                <div class="header__contact">
                    <img src="images/phone.png" alt="">
                    {{ setting('site.telephone') }}
                </div>
                <div class="header__contact">
                    <img src="images/mail.png" alt="">
                    {{ setting('site.mail') }}
                </div>
            </div>
        </div>
        <div class="header__banner">
            <div class="header__banner-text">
                <div class="header__text-wrapper">
                    <div class="header__title">
                        {{ $banner->name }}
                    </div>
                    <div class="header__description">
                        {{ $banner->description }}
                    </div>
                    <a href="https://wa.me/{{ setting('site.whatsapp') }}" class="header__btn">
                        Оценить
                    </a>
                </div>
            </div>
            <div class="header__banner-image">
                <img src="/storage/{{ $banner->image }}" alt="">
            </div>
        </div>
        <div class="header__features-wrapper">
            <div class="header__features">
                @foreach ($features as $feature)
                <div class="header__feature">
                    <div class="header__img-wrapper">
                        <img src="/storage/{{ $feature->image }}" alt="">
                    </div>
                    {{ $feature->text }}
                </div>
                @endforeach
            </div>
        </div>
    </div>
</header>

<div class="about-us">
    <div class="container">

        <div class="about-us__wrapper">
            <div class="about-us__image">
                <img src="/storage/{{ $about_us->image }}" alt="">
            </div>
            <div class="about-us__info">
                <div class="about-us__sub-title">
                    О нас <img src="images/line.png" alt="">
                </div>
                <div class="about-us__title">
                    <h1>
                        {{ $about_us->title }}
                    </h1>
                </div>
                <div class="about-us__description">
                    {{ $about_us->description }}
                </div>
            </div>
        </div>
    </div>

</div>

<div class="arrivals">
    <div class="container">
        <div class="arrivals__title">
            <h1>Новые поступления</h1>
        </div>
        <div class="arrivals__items">
            @foreach ($products as $product)
            <div class="arrivals__item">
                <img src="/storage/{{ $product->image }}" alt="">
                <span class="arrivals__item-name">
                    {{ $product->name }}
                </span>
                <div class="arrivals__item-price">
                    <h1>
                        {{ $product->price }} $
                    </h1>
                </div>
            </div>
            @endforeach
        </div>
        <div class="arrivals__btn-block">
            <a href="#" class="arrivals__btn">
                Смотреть все
            </a>
        </div>
    </div>
</div>

<div class="catalog">
    <div class="container">
        <div class="catalog__title">
            <h1>Мы выкупаем</h1>
        </div>
        <div class="catalog__tabs">
            @foreach ($buyout_products as $buyout_product)
            <button class="catalog__tab @if($loop->first) tab-active @endif" onclick="changeTab(event,'Tab{{ $loop->index }}')">
                {{ $buyout_product->title }}
            </button>
            @endforeach
        </div>

        @foreach ($buyout_products as $buyout_product)
            <div class="catalog__tab-content" id="Tab{{ $loop->index }}">
                <div class="catalog__wrapper">

                    <div class="catalog__info">
                        <h1>
                            {{ $buyout_product->name }}
                        </h1>
                        <span class="catalog__text">
                            {!! $buyout_product->text !!}
                        </span>
                        <div class="catalog__btn-block">
                            <a href="https://wa.me/{{ setting('site.whatsapp') }}" class="catalog__btn">
                                Оценить часы
                            </a>
                        </div>
                    </div>

                    <div class="catalog__image">
                        <img src="/storage/{{ $buyout_product->image }}" alt="">
                    </div>

                </div>
            </div>
        @endforeach

    </div>
</div>

<div class="buyback">
    <div class="container">
        <div class="buyback__title">
            <h1>
                Как проходит выкуп
            </h1>
        </div>
        <div class="buyback__items">
            @foreach ($buyouts as $buyout)
            <div class="buyback__item">
                <div class="buyback__number">
                    <div>
                        0{{ $buyout->id }}
                    </div>
                </div>
                <div class="buyback__info">
                    <img src="/storage/{{ $buyout->image }}" alt="">
                    {{ $buyout->text }}
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="feedback">
    <div class="feedback__image">
        <img src="/storage/{{ $selling->image }}" alt="">
    </div>
    <div class="feedback__info">
        <div class="feedback__info-wrapper">
            <div class="feedback__title">
                <h1>
                    {{ $selling->name }}
                </h1>
            </div>
            <div class="feedback__description">
                <p>
                    {{ $selling->description }}
                </p>
            </div>
            <div class="feedback__btn-wrapper">
                <a href="https://wa.me/{{ setting('site.whatsapp') }}" class="feedback__btn">
                    Отправить фото на оценку
                </a>
            </div>
        </div>
    </div>
</div>

<div class="features">
    <div class="container">
        <div class="features__title">
            <h1>
                Как проходит выкуп
            </h1>
        </div>
        <div class="features__items">
            @foreach ($advantages as $advantage)
            <div class="features__item">
                <div class="features__info">
                    <div class="features__number">
                        <div>
                            <img src="/storage/{{ $advantage->image }}" alt="">
                        </div>
                    </div>
                    <h1>{{ $advantage->name }}</h1>
                    <p>
                        {{ $advantage->description }}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</div>

<div class="brands">
    <div class="container">
        <div class="brands__items">
            @foreach ($brands as $brand)
            <div class="brands__item">
                <p>{{ $brand->name }}</p>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="footer__sections-wrapper">
            <div class="footer__logo">
                <img src="images/logo.png" alt="">
            </div>
            <div class="footer__navigation">
                <div class="footer__title">
                    Навигация
                </div>

                <div class="footer__links-wrapper">
                    <div class="footer__links">
                        <div class="footer__link" onclick="document.querySelector('.about-us').scrollIntoView({
  behavior: 'smooth'
});">
                            О нас
                        </div>
                        <div class="footer__link" onclick="document.querySelector('.arrivals').scrollIntoView({
  behavior: 'smooth'
});">
                            Купить изделие
                        </div>
                        <div class="footer__link" onclick="document.querySelector('.catalog').scrollIntoView({
  behavior: 'smooth'
});">
                            Мы выкупаем
                        </div>
                        <div class="footer__link" onclick="document.querySelector('.buyback').scrollIntoView({
  behavior: 'smooth'
});">
                            Как проходит выкуп
                        </div>
                    </div>
                    <div class="footer-links" style="margin-left: 40px">
                        <div class="footer__link" onclick="document.querySelector('.feedback').scrollIntoView({
  behavior: 'smooth'
});">
                            Оценить изделие
                        </div>
                        <div class="footer__link"onclick="document.querySelector('.features').scrollIntoView({
  behavior: 'smooth'
});">
                            Наши преимущества
                        </div>
                        <div class="footer__link" onclick="document.querySelector('.brands').scrollIntoView({
  behavior: 'smooth'
});">
                            Бренды
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__schedule">
                <div class="footer__title">
                    График работы
                </div>
                <div class="footer__items">
                    <div class="footer__item">
                        {!! setting('site.schedule') !!}
                    </div>
                </div>
            </div>
            <div class="footer__contacts">
                <div class="footer__title">
                    Контакты
                </div>
                <div class="footer__items">
                    <div class="footer__item">
                        <img src="images/adress.png" alt="">
                        {{ setting('site.address') }}
                    </div>
                    <div class="footer__item">
                        <img src="images/phone.png" alt="">
                        {{ setting('site.telephone') }}
                    </div>
                    <div class="footer__item">
                        <img src="images/mail.png" alt="">
                        {{ setting('site.mail') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-contacts">
    <div class="footer-contacts__wrapper container">
        <div class="footer-contacts__time">
            © 2021. Watches. Все права защищены
        </div>
        <div class="footer-contacts__socials">
            <div class="footer-contacts__images">
                @foreach ($socials as $social)
                    <div class="contacts__image">
                        <a href="{{ $social->name }}"><img src="/storage/{{ $social->image_bottom }}" alt=""></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<script>
    tabcontent = document.getElementsByClassName("catalog__tab-content");
    for (i = 1; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    function changeTab(evt,TabName) {
        // Declare all variables
        var i,tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("catalog__tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("catalog__tab");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" tab-active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(TabName).style.display = "block";
        evt.currentTarget.className += " tab-active";
    }
</script>
</body>
</html>