<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index', [
        'socials'=>App\Models\Social::all(),
        'banner'=>App\Models\Banner::first(),
        'features'=>App\Models\Feature::all(),
        'about_us'=>App\Models\AboutUs::first(),
        'products'=>App\Models\Product::all(),
        'buyouts'=>App\Models\Buyout::all(),
        'brands'=>App\Models\Brand::all(),
        'selling'=>App\Models\Selling::first(),
        'advantages'=>App\Models\Advantage::all(),
        'buyout_products'=>App\Models\BuyoutProduct::all(),
        ]);
    });


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
